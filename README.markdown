[//]: # (Filename: README.markdown)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <ivanruvalcaba[at]disroot[dot]org>)
[//]: # (Created: 11 jul 2020 12:19:25)
[//]: # (Last Modified: 12 jul 2020 14:14:38)

# Notas en «Gemtext»

Este repositorio contiene anotaciones, ideas e inquietudes, de carácter estrictamente personal, plasmadas utilizando el lenguaje de marcado ligero [Gemtext](https://portal.mozz.us/gemini/gemini.circumlunar.space/docs/gemtext.gmi) para su posible futura reutilización o publicación.

## Motivación

Al centrarse mis intereses técnicos casi exclusivamente en [el protocolo de Internet Gemini](https://gemini.circumlunar.space/) y en [el lenguaje de programación Nim](https://nim-lang.org/), tengo la oportunidad de compaginar mis otras múltiples aficiones, como la redacción por citar un ejemplo, de forma óptima a mi nuevo flujo de vida.

## Autor

* **Iván Ruvalcaba** <ivanruvalcaba[at]disroot[dot]org>.

### Social

* *XMPP* <ivanruvalcaba[at]disroot[dot]org>.
* [Mastdon](https://mastodon.social/@ivanruvalcaba/).

### Blogs

* [Write.as](https://write.as/ivanruvalcaba/).
* [Gemlog](https://proxy.vulpes.one/gemini/gemlog.blue/users/ivanruvalcaba/).

## Licencia

Copyright (c) 2020 — Iván Ruvalcaba. *A no ser que se indique explícitamente lo contrario, el contenido de este repositorio se encuentra sujeto a los términos de la licencia [Creative Commons Reconocimiento-SinDerivados 4.0 Internacional (CC BY-ND 4.0 International)](https://creativecommons.org/licenses/by-nd/4.0/deed.es_ES).*

![CC BY-ND 4.0 International](assets/img/cc-by-nd-80x15.png)
